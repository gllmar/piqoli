##### https://www.juanmtech.com/samba-file-sharing-raspberry-pi/

echo "
### install dependencies
### at wins config no?
"
sudo apt-get update 
sudo apt-get install samba samba-common-bin


echo "
### add user pi to sambagroup and change password
"

sudo smbpasswd -a pi

echo"
TODO  Config /etc/smb.conf

[global]
    workgroup = WORKGROUP
    encrypt passwords = yes
    wins support = yes
    log level = 1
    max log size = 1000
    read only = no

[homes]
    comment = Home Directories
    browseable = yes
    valid users = %S
"

echo "
### restart samba service
"

sudo service smbd restart
