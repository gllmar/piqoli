#!/bin/bash

###@ from  https://raspberrytips.com/install-obs-studio-raspberry-pi/
CACHE_DIR=~/.cache/maja/
# Titre
## etapes
### build dependencies
####  apt-get


function get_dependencies_apt(){
sudo apt install \
build-essential checkinstall cmake git \
libmbedtls-dev libasound2-dev \
libavcodec-dev libavdevice-dev libavfilter-dev libavformat-dev \
libavutil-dev libcurl4-openssl-dev libfontconfig1-dev libfreetype6-dev \
libgl1-mesa-dev libjack-jackd2-dev libjansson-dev libluajit-5.1-dev \
libpulse-dev libqt5x11extras5-dev libspeexdsp-dev libswresample-dev \
libswscale-dev libudev-dev libv4l-dev libvlc-dev libx11-dev libx11-xcb1 \
libx11-xcb-dev libxcb-xinput0 libxcb-xinput-dev libxcb-randr0 libxcb-randr0-dev \
libxcb-xfixes0 libxcb-xfixes0-dev libx264-dev libxcb-shm0-dev libxcb-xinerama0-dev \
libxcomposite-dev libxinerama-dev pkg-config python3-dev \
qtbase5-dev libqt5svg5-dev swig
}


#### from sources

##### FFmpeg AAC
#mkdir -p "$CACHE_DIR"

#mkdir -p "$CACHE_DIR/$PKG_NAME"
#cd  "$CACHE_DIR"/"$PKG_NAME"
#wget "$PKG_URL"
#sudo dpkg -i "$PKG_NAME".deb

#function get_deb_dpkg(){
#	DEB_URL"$1"
#	CACHE_DIR="$2"
#
#	mkdir -p "$CACHE_DIR/$PKG_NAME"
#	cd  "$CACHE_DIR"/"$PKG_NAME"
#	wget "$PKG_URL"
#	sudo dpkg -i "$PKG_NAME".deb
#			
#}


#PKG_NAME="libfdk-aac2_2.0.1-1_armhf"
#PKG_URL="http://ftp.debian.org/debian/pool/non-free/f/fdk-aac/libfdk-aac2_2.0.1-1_armhf.deb"



#wget "http://ftp.debian.org/debian/pool/non-free/f/fdk-aac/libfdk-aac-dev_2.0.1-1_armhf.deb"
#sudo dpkg -i libfdk-aac-dev_2.0.1-1_armhf.deb


### OBS from sources
####

function get_obs_studio_git(){
	
OBS_STUDIO_GIT_DIR=~./cache/maja/obs-studio-git
mkdir -p  "$OBS_DIR"
cd  "$OBS_DIR"
mkdir build
cd build
}
function build_obs_studio()
{
cd  "$OBS_DIR/build"
cmake -DUNIX_STRUCTURE=1 -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
}

##### exec

#get_dependencies
get_obs_studio_git
build_obs_studio


