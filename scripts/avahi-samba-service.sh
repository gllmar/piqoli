
echo"
### creating service
"
cat <<EOT >> samba.service
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
    <name replace-wildcards="yes">%h filer</name>
    <service>
        <type>_smb._tcp</type>
        <port>445</port>
    </service>
</service-group>
EOT

echo "
### overwriting /etc/avahi/services/samba.service
"
sudo mv samba.service /etc/avahi/services/samba.service


echo "
### restarting avahi service
" 
sudo systemctl restart avahi-daemon
